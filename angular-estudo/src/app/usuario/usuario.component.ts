import { Component, OnInit } from '@angular/core';
import {  FormsModule, NgForm } from '@angular/forms';
import { Http, Response, Headers } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';


@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {

  constructor(private http: Http) { }
    mensagemdeconfirmacao:string = "O usuário foi cadastrado";
    foiInserido:boolean = false;
    usuarioObject:object = [];

    addNovoUser = function(usuario){
      this.usuarioObject = {
          "nome": usuario.nome,
          "login": usuario.login,
          "email": usuario.email,
          "senha": usuario.senha
      }
      usuario.nome = "";
      this.http.post("http://localhost:3333/usuarios/",this.usuarioObject).subscribe((res:Response)=> {
        this.foiInserido= true;
      });
    }
  ngOnInit() {
  }

}
