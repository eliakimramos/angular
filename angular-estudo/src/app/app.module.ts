import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpModule, Response, Headers } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import {  FormsModule, NgForm } from '@angular/forms';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { UpdateUsuarioComponent } from './update-usuario/update-usuario.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    UsuarioComponent,
    UpdateUsuarioComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot([
        {path: '', component: HomeComponent},
        {path: 'usuario', component: UsuarioComponent},
        {path: 'ateracao-usuario/:id', component: UpdateUsuarioComponent}
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
