import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private http: Http) { }
  id:number;
  private baseDbPath = "http://localhost:3333/usuarios/";
  private headers = new Headers({'Content-Type':'application/json'});
  usuarios = [];
  fetchData = function(){
      this.http.get("http://localhost:3333/usuarios").subscribe(
        (res: Response) => {
          this.usuarios = res.json();
        }
      )
  }

  deletar = function(id){ 
    if(confirm("Você tem certeza que vai apagar esse usuário?")){
        const url = this.baseDbPath+`${id}`;
        
        return this.http.delete(url, {'headers': this.headers}).toPromise()
                .then(()=>{
                  this.fetchData();
                })
      }
  }

  ngOnInit() {
    this.fetchData();
  }

}
