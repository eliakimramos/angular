import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { Route } from '@angular/router/src/config';

@Component({
  selector: 'app-update-usuario',
  templateUrl: './update-usuario.component.html',
  styleUrls: ['./update-usuario.component.css']
})
export class UpdateUsuarioComponent implements OnInit {
  private baseDbPath = "http://localhost:3333/usuarios/";
  mensagemdeconfirmacao:string = "O usuário foi Alterado";
  foiAlterado:boolean = false;
  id: number;
  data:object = {};
  usuarios = [];
  usuarioObj:object = {};
  exist: boolean;
  private headers = new Headers({'Content-Type':'application/json'});

  constructor(private router: Router, private route: ActivatedRoute, private http:Http) { }

  alterarUser(usuario){
    this.usuarioObj = {
          "nome": usuario.nome,
          "email": usuario.email,
          "login":usuario.login,
          "senha" : usuario.senha
    };
    const url = this.baseDbPath+`${this.id}`;
    this.http.put(url, JSON.stringify(this.usuarioObj), {headers: this.headers})
        .toPromise()
        .then(()=> {
          this.router.navigate(['/']);
        })

  }

  ngOnInit() {
    this.route.params.subscribe(params=>{
      this.id = +params['id'];
    });
    this.http.get("http://localhost:3333/usuarios").subscribe(
      (res: Response) => {
        this.usuarios = res.json();
        for(var i =0; i < this.usuarios.length; i++){
          if(parseInt(this.usuarios[i].id) === this.id){
            this.exist = true;
            this.data = this.usuarios[i];
            break;
          }else{
              this.exist = false;
          }
        }
      }
    )
  }


}
